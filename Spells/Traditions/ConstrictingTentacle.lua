--[[
    Script Name    : Spells/Traditions/ConstrictingTentacle.lua
    Script Author  : neatz09
    Script Date    : 2020.08.18 01:08:23
    Script Purpose : 
                   : 
--]]

-- Info from spell_display_effects (remove from script when done)
-- Roots target
-- Decreases Attack Speed of target by 6.7 - 11.3
-- Epic targets gain an immunity to Root effects of 30.0 seconds and duration is reduced to 3.3 seconds.
-- Resistibility increases against targets higher than level 29.

function cast(Caster, Target)
    Say(Target, "Hah, nice try! That's not implemented yet!")
end
